Completed:
-
- MiningMPI.cpp
- integer.h
- integer.cpp

Compile with `mpic++ MiningMPI.cpp integer.cpp -lcrypto -lssl`
Run with `mpirun --hostfile /usr/local/pub/ph/TardisClusterWithGPU.txt --prefix /usr/local a.out`

WIP:
-
- cuda.cpp
- main.cu
- sha256.cuh