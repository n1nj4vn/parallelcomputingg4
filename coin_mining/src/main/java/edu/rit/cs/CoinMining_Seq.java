package edu.rit.cs;

import java.util.Arrays;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import mpi.MPI;
import mpi.MPIException;

public class CoinMining_Seq {

  /**
   * convert byte[] to hex string
   *
   * @return hex string
   */
  private static String bytesToHex(byte[] hash) {
    StringBuffer hexString = new StringBuffer();
    for (int i = 0; i < hash.length; i++) {
      String hex = Integer.toHexString(0xff & hash[i]);
      if (hex.length() == 1) {
        hexString.append('0');
      }
      hexString.append(hex);
    }
    return hexString.toString();
  }

  /**
   * get a sha256 of the input string
   *
   * @return resulting hash in hex string
   */
  public static String SHA256(String inputString) {
    try {
      MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
      return bytesToHex(sha256.digest(inputString.getBytes(StandardCharsets.UTF_8)));
    } catch (NoSuchAlgorithmException ex) {
      System.err.println(ex.toString());
      return null;
    }
  }

  /**
   * perform the proof-of-work
   *
   * @param blockHash hash of the blockinfo
   * @param targetHash target hash
   * @return nonce (a 32-bit integer) that satisfies the requirements
   */
  public static long pow(String blockHash, String targetHash, int start, int end) {
    // System.out.println("Performing POW; Start: " + start + " End: " + end);
    String tmp_hash;
    long finalNonce = Long.MIN_VALUE;
    for (int nonce = start; nonce <= end; nonce++) {
      tmp_hash = SHA256(SHA256(blockHash + String.valueOf(nonce)));
      if (targetHash.compareTo(tmp_hash) > 0) {
        finalNonce = nonce;
        // System.out.println("BREAK-NONCE: " + finalNonce);
        break;
      }
    }
    return finalNonce;
  }

  public static String HexValueDivideBy(String hexValue, int val) {
    BigInteger tmp = new BigInteger(hexValue, 16);
    tmp = tmp.divide(BigInteger.valueOf(val));
    String newHex = bytesToHex(tmp.toByteArray());
    while (newHex.length() < hexValue.length()) {
      newHex = '0' + newHex;
    }
    return newHex;
  }

  public static String HexValueMultipleBy(String hexValue, int val) {
    BigInteger tmp = new BigInteger(hexValue, 16);
    tmp = tmp.multiply(BigInteger.valueOf(val));
    String newHex = bytesToHex(tmp.toByteArray());
    while (newHex.length() < hexValue.length()) {
      newHex = '0' + newHex;
    }
    return newHex;
  }

  public static void main(String[] args) throws MPIException {
    MPI.Init(args);
    
    int rank = MPI.COMM_WORLD.getRank(), size = MPI.COMM_WORLD.getSize();
    String hostname = MPI.getProcessorName();
    if(rank == 0){
      System.out.println(
          "CoinMining_Seq: Number of tasks = " + size + ", My rank =" + rank + ", Running on "
              + hostname);
    }

    String tmpBlockHash = "";
    String tmpTargetHash = "";
    int[] sendS = new int[size];
    int[] sendE = new int[size];
    int[] recvS = new int[1];
    int[] recvE = new int[1];
    double totalNonce = 4294967295.0;
    int workSize = (int) (totalNonce / size);
    long[] buffOne = new long[1];
    long[] buffTwo = new long[1];
    double[] timeBuf = new double[1];

    // number of blocks to be generated or number of rounds; default to 5
    int numberOfBlocks = 5;
    // average block generation time, default to 30 Secs.
    double avgBlockGenerationTimeInSec = 30.0;


    // init block hash
    String initBlockHash = SHA256("CSCI-654 Foundations of Parallel Computing");
    // init target hash
    String initTargetHash = "0000992a6893b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";

    if(rank == 0){
      System.out.println("Initial Block Hash:  " + initBlockHash);
      System.out.println("Initial Target Hash: " + initTargetHash);
      System.out.println();
    // Chunk up the work based on the number of workers available on root node
      int temp = Integer.MIN_VALUE;
        for (int i = 0; i < size - 1; i++) {
          sendS[i] = temp;
          sendE[i] = temp + workSize;
          temp = temp + workSize;
          //System.out.println("i: " + i + ", temp: " + temp);
        }

        sendS[size - 1] = temp;
        sendE[size - 1] = Integer.MAX_VALUE;

        // System.out.println("sendS: " + Arrays.toString(sendS));
        // System.out.println("sendE: " + Arrays.toString(sendE));
    }

    MPI.COMM_WORLD.scatter(sendS, 1, MPI.INT, recvS, 1, MPI.INT, 0);
    MPI.COMM_WORLD.scatter(sendE, 1, MPI.INT, recvE, 1, MPI.INT, 0);

    int currentBlockID = 1;
    tmpBlockHash = initBlockHash;
    tmpTargetHash = initTargetHash;
    MyTimer myTimer = new MyTimer("CurrentBlockID:" + currentBlockID);

    while (currentBlockID <= numberOfBlocks) {
      if (rank == 0) {
        myTimer.start_timer();
      }

      //System.out.println("recvS[0]: " + recvS[0] + " recvE[0]: " + recvE[0]);
      buffOne[0] = pow(tmpBlockHash, tmpTargetHash, recvS[0], recvE[0]);

      MPI.COMM_WORLD.allReduce(buffOne, buffTwo, 1, MPI.LONG, MPI.MAX);

      int nonce = (int)buffTwo[0];

      if (rank == 0) {
        myTimer.stop_timer();
        myTimer.print_elapsed_time();
        timeBuf[0] = myTimer.get_elapsed_time_in_sec();
      }
        // found a new block
        tmpBlockHash = SHA256(tmpBlockHash + "|" + nonce);

        MPI.COMM_WORLD.bcast(timeBuf, 1, MPI.DOUBLE, 0);
        // update the target
        if (timeBuf[0] < avgBlockGenerationTimeInSec) {
          tmpTargetHash = HexValueDivideBy(tmpTargetHash, 2);
        } else {
          tmpTargetHash = HexValueMultipleBy(tmpTargetHash, 2);
        }

      if(rank == 0){
        System.out.println("Nonce: " + nonce);
        System.out.println("New Block Hash:  " + tmpBlockHash);
        System.out.println("New Target Hash: " + tmpTargetHash);
        System.out.println();
     }
        currentBlockID++;
    }

    MPI.Finalize();
  }
}