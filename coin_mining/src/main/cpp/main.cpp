#include <iostream>
#include <climits>
#include <cstring>
#include <string>
#include "fmt/format.h"
#include <thread> 
#include "sha256.h"
#include <vector>
#include <mutex>

using namespace std;

// #include <openssl/sha.h>

// string sha256(const string str)
// {
//     unsigned char hash[SHA256_DIGEST_LENGTH];
//     SHA256_CTX sha256;
//     SHA256_Init(&sha256);
//     SHA256_Update(&sha256, str.c_str(), str.size());
//     SHA256_Final(hash, &sha256);
//     // stringstream ss;
//     // for(int i = 0; i < SHA256_DIGEST_LENGTH; i++)
//     // {
//     //     ss << hex << setw(2) << setfill('0') << (int)hash[i];
//     // }
//     // return ss.str();

//     static const char characters[] = "0123456789abcdef";
// 	std::string result (SHA256_DIGEST_LENGTH * 2, ' ');
// 	for(int i = 0; i < SHA256_DIGEST_LENGTH; i++)
// 	{
// 	    result[2*i] = characters[(unsigned int) hash[i] >> 4];
// 	    result[2*i+1] = characters[(unsigned int) hash[i] & 0x0F];
// 	}
// 	return result;
// }

class NonceStatus{
	private:
		int nonce;
		bool nonceFound = false;
		string resultHash = "";
	public:
		bool getNonceStatus(){
			return nonceFound;
		}
		int getNonceValue(){
			return nonce;
		}
		string getResultHash(){
			return resultHash;
		}

		void setNonceStatus(){
			nonceFound = true;
		}
		void setNonceValue(int nonce){
			this->nonce = nonce;
		}
		void setResultHash(string resultHash){
			this->resultHash = resultHash;
		}
};

void powFunc(string blockHash, string targetHash, int min_val, int max_val, NonceStatus& nonceStatus){
	SHA256 sha256;
	string tmp_hash;
	int nonce;
    for(nonce = min_val; nonce < max_val; nonce++){
    	if(nonceStatus.getNonceStatus()){
    		break;
    	}
        // tmp_hash = sha256(sha256(fmt::format("{}{}", blockHash, nonce)));
        tmp_hash = sha256(sha256(blockHash + to_string(nonce)));

        // if(strcmp(tmp_hash.c_str(), targetHash.c_str()) < 0){
        if(tmp_hash.compare(targetHash) < 0){
    		nonceStatus.setNonceValue(nonce);
    		nonceStatus.setResultHash(tmp_hash);
    		nonceStatus.setNonceStatus();
            break;
        }
    }
}

int main(int argc, char *argv[])
{   
    // string input = "CSCI-654 Foundations of Parallel Computing";
    SHA256 sha256;
    string input = "Some random string to generate a block hash.";
    string blockHash = sha256(input);
    cout << "blockHash: " << blockHash << endl;
    string targetHash = "000000938023b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";
    NonceStatus nonceStatus;
    int numThreads = 8;
    int partitionSize = (int) (((long) INT_MAX - (long) INT_MIN) / numThreads);
    vector<thread> powThreads;
    for (int i=0; i<numThreads; i++){	
    	powThreads.push_back(thread(powFunc, blockHash, targetHash, INT_MIN+partitionSize*i, INT_MIN+partitionSize*(i+1), ref(nonceStatus)));
    }
	
	for (thread & th : powThreads)
	{
		th.join();
	}

    cout<<"Resulting Hash: "<<nonceStatus.getResultHash()<<endl;
    cout<<"Nonce: "<<nonceStatus.getNonceValue()<<endl;

    return 0;
}