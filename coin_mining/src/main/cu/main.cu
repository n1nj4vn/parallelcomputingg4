/**
    CSCI-654-01 Foundations of Parallel Computing
    File: main.cu
    Purpose:

    @author John Tran - jxt5551@rit.edu
    @version 1.0 11/29/2019
*/
#include <iostream>
#include <algorithm>
#include "sha256.h"
#include <string>

using namespace std;

// Accessible by ALL CPU and GPU functions !!!
__managed__ int finalNonce = 0;
__managed__ bool nonceStatus = false;

// Kernel function
__global__
void pow(string blockHash, string targetHash, int N) {
    // index = block index * number of threads per block + thread index
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    // stride  = number threads per block * number of block per grid
    int stride = blockDim.x * gridDim.x;

    SHA256 sha256;
    string tmp_hash;

    for (int i = index; i < N; i += stride) {
        tmp_hash = sha256(sha256(blockHash + to_string(i)));
        if (!nonceStatus) {
            if (tmp_hash.compare(targetHash) < 0) {
                atomicAdd(&finalNonce, i);
                nonceStatus = true;
                break;
            }
        }
    }
}

__device__ int cust_atoi(char *src) {
    int res = 0;
    int i = 0;
    while (src[i] != 0) {
        res = res * 10 + str[i] - '0';
        i++;
    }
    return res;
}

__device__ char *cust_strcpy(char *dest, const char *src) {
    int i = 0;
    do {
        dest[i] = src[i];
    } while (src[i++] != 0);
    return dest;
}

__device__ char *cust_strcat(char *dest, const char *src) {
    int i = 0;
    while (dest[i] != 0) i++;
    cust_strcpy(dest + i, src);
    return dest;
}

__device__ int cust_strcmp(const char *str_a, const char *str_b, unsigned len = 256) {
    int match = 0;
    unsigned i = 0;
    unsigned done = 0;
    while ((i < len) && (match == 0) && !done) {
        if ((str_a[i] == 0) || (str_b[i] == 0)) done = 1;
        else if (str_a[i] != str_b[i]) {
            match = i + 1;
            if ((int) str_a[i] - (int) str_b[i]) < 0) match = 0 - (i + 1);
        }
        i++;
    }
    return match;
}

// host code
/*
 * Main function
 */
int main(int argc, char *argv[]) {
    SHA256 sha256;
    string input = "CSCI-654 Foundations of Parallel Computing";
    string blockHash = sha256(input);
    cout << "blockHash: " << blockHash << endl;
    string targetHash = "000000938023b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";
    cout << "targetHash: " << targetHash << endl;

    int N = (int) ((long) INT_MAX - (long) INT_MIN);
    // Run kernel on some number of points on the GPU
    int blockSize = 256;
    int numBlocks = (N + blockSize - 1) / blockSize;
    pow << < numBlocks, blockSize >> > (blockHash, targetHash, N);

    // Wait for GPU to finish before accessing on host
    cudaDeviceSynchronize();

    string tmpBlockHash = sha256(blockHash + to_string(finalNonce));

    cout << "Resulting Hash: " << tmpBlockHash << endl;
    cout << "Nonce: " << finalNonce << endl;

    return 0;
}
