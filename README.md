# Group 4 Coin Mining

Completed
-
- Java Multi-thread & Multi-node `\parallelcomputingg4\coin_mining\src\main\java\edu\rit\cs`
- C++ Multi-thread: `\parallelcomputingg4\coin_mining\src\main\cpp`
- C++ Multi-thread & Multi-node: `\parallelcomputingg4\coin_mining\src\main\ompi_cpp`

WIP
-
- GPU: `\parallelcomputingg4\coin_mining\src\main\cu`